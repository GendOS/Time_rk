#include "mainwindow.h"
#include <QJsonObject>
#include <QJsonDocument>
#include <QDebug>
#include <QFile>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    setupUi(this);
    QFile logFile("logfile.txt");
    QTextStream logStream(&logFile);
    QJsonDocument jsonDoc;
    date  = new QDate;
    time = new QTime;
    *date = QDate::currentDate();
    *time = QTime::currentTime();

    if (logFile.open(QFile::WriteOnly|QFile::Append)){
        qDebug()<<"\n LogFile opened";
    }
    else qDebug()<<"\n Error cant open logfile.txt";

    QFile configFile ("config.json");
    if (configFile.exists()){
        configFile.open(QFile::ReadOnly);
        logStream<<"\n"<<date->toString()<<" "<<time->toString();;

        logStream<<"\n\n config.json opened";
        jsonDoc = QJsonDocument().fromJson(configFile.readAll());
        txtConfig->setText(jsonDoc.toJson());
        QJsonObject obj = jsonDoc.object();
        logStream<<"\n"<<time->toString();

        logStream<<"\n"<<"ip:port = "<<obj.value("ip").toVariant().toString()<<\
                   ":"<<obj.value("port").toVariant().toString();
        logStream<<"\n"<<time->toString();

        logStream<<"\n"<<"frequency = "<<obj.value("frequency").toVariant().toString();

        logFile.flush();
        logFile.close();
        logFile.open(QFile::ReadOnly);
        txtLog->setText(logFile.readAll());
    }
    else{
        qDebug()<<"Filr not op[ened \n";
        logStream<<"File not opened \n";
    }

    configFile.close();
    logFile.close();


}

MainWindow::~MainWindow()
{

}
